#!/usr/bin/env bash

set -ex

IP_ADDRESS=$(curl http://169.254.169.254/latest/meta-data/local-ipv4)

sudo rm -rf /var/lib/vault/*

DEBIAN_FRONTEND=noninteractive \
  sudo apt-get update && \
  sudo apt-get upgrade -y && \
  sudo apt-get install unzip -y

curl -L https://releases.hashicorp.com/vault/0.11.1/vault_0.11.1_linux_amd64.zip \
  -o /tmp/vault_0.11.1_linux_amd64.zip && \
  unzip -uo /tmp/vault_0.11.1_linux_amd64.zip -d /tmp/ && \
  sudo cp -pv /tmp/vault /usr/local/bin/ && \
  vault --version

sudo useradd -r -d /var/lib/vault -s /bin/nologin vault || true
sudo install -o vault -g vault -m 750 -d /var/lib/vault

# for mlock/munlock()'ing
sudo /sbin/setcap cap_ipc_lock=+ep /usr/local/bin/vault

tee /etc/systemd/system/vault.service >/dev/null <<-'HD'
[Unit]
Description=Hashicorp Vault Service
Documentation=https://vaultproject.io/docs/
Requires=network-online.target
After=network-online.target
ConditionFileNotEmpty=/etc/vault.d/vault.hcl

[Service]
User=vault
Group=vault
Restart=on-failure
Environment=GOMAXPROCS=nproc
ExecStart=/usr/local/bin/vault server -config="/etc/vault.d/vault.hcl"
ExecReload=/bin/kill -HUP $MAINPID
ExecStop=/usr/local/bin/vault operator step-down
LimitMEMLOCK=infinity
KillSignal=SIGTERM

[Install]
WantedBy=multi-user.target

HD

sudo install -d -m 0755 -o vault -g vault /etc/vault.d/

tee /etc/vault.d/vault.hcl >/dev/null <<HD
backend "file" {
  path = "/var/lib/vault"
}
ui = true
listener "tcp" {
  address = "127.0.0.1:8200"
  cluster_address = "127.0.0.1:8201"
  tls_disable = 1
}
HD

tee /etc/vault.d/vault.consul.hcl >/dev/null <<HD
backend "consul" {
  path = "vault/"
  address = "${IP_ADDRESS}:8500"
  cluster_addr = "https://${IP_ADDRESS}:8201"
  redirect_addr = "http://${IP_ADDRESS}:8200"
}

listener "tcp" {
  address = "${IP_ADDRESS}:8200"
  cluster_address = "${IP_ADDRESS}:8201"
  tls_disable = 1
}
HD

sudo chown -R vault:vault /etc/vault.d /usr/local/bin/vault
sudo chmod 0640 /etc/vault.d/*
sudo systemctl daemon-reload
sudo systemctl restart vault