# Source 
https://www.vaultproject.io/guides/operations/generate-root.html

# Generate OTP
```bash
$ vault operator generate-root -generate-otp
mOXx7iVimjE6LXQ2Zna6NA==
```

# Init with OTP
```bash
$ vault operator generate-root -init -otp=mOXx7iVimjE6LXQ2Zna6NA==
Nonce              f67f4da3-4ae4-68fb-4716-91da6b609c3e
Started            true
Progress           0/5
Complete           false
```

# Get keys (2/2)

Assuming 2 keys needed for quorum, decrypt key 1 (Shamir secret-key sharing scheme):
```bash
$ cat demo-vault-cluster.json | \
    jq -r ".keys_base64[0]"  | \
    base64 -d | \
    gpg --decrypt
abcd...
```

Then, decrypt key 2 (Shamir secret-key sharing scheme)
```bash
$ cat demo-vault-cluster.json | \
    jq -r ".keys_base64[1]"  | \
    base64 -d | \
    gpg --decrypt
abcd.....
```

# Generate proper root using unseal key(s) 
```bash
$ vault operator generate-root
Root generation operation nonce: f67f4da3-4ae4-68fb-4716-91da6b609c3e
Unseal Key (will be hidden): abcd...
```

# Get the root token using encoded key and the OTP
```bash
$ vault operator generate-root \
    -decode=IxJpyqxn3YafOGhqhvP6cQ== \
    -otp=mOXx7iVimjE6LXQ2Zna6NA==

```